import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

// import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { MainPage } from '../pages/main/main';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = LoginPage;
  profileData: any = [
    { name: "sonal", number: 851146978 }
  ];

  tab1 = HomePage;
  tab2 = HomePage;
  info: any = [];
  @ViewChild(Nav) nav: Nav;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    public http: Http, ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    let localData = http.get('assets/data.json').map(res => res.json().data);
    localData.subscribe(responce => {
      console.log(responce)
      this.info = responce;
    })
    console.log(this.profileData);

  }
  totglebtn(i) {
    this.info[i].open = !this.info[i].open;
  }
  toggleItem(i, j, child) {
    console.log(child);
    debugger
    this.nav.push(MainPage)
    this.info[i].children[j].open = !this.info[i].children[j].open;

    for (let i1 = 0; i1 < this.info.length; i1++) {
      this.info[i1]['selected'] = false;
    }
    this.info[j]['selected'] = true;
    console.log(this.info);
  }

}

