import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http'

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { VerifyMePage } from '../pages/verify-me/verify-me';
import { DashbordPage } from '../pages/dashbord/dashbord';
import { CityListPage } from '../pages/city-list/city-list';
import { DashbordShoppingPage } from '../pages/dashbord-shopping/dashbord-shopping';
import { SuperTabsModule } from 'ionic2-super-tabs';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    VerifyMePage,
    DashbordPage,
    CityListPage,
    DashbordShoppingPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    SuperTabsModule.forRoot(),
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition'
    }),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    VerifyMePage,
    DashbordPage,
    CityListPage,
    DashbordShoppingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
