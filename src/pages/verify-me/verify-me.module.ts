import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifyMePage } from './verify-me';

@NgModule({
  declarations: [
    VerifyMePage,
  ],
  imports: [
    IonicPageModule.forChild(VerifyMePage),
  ],
})
export class VerifyMePageModule {}
