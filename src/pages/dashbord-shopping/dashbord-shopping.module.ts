import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashbordShoppingPage } from './dashbord-shopping';
import { SuperTabsModule } from 'ionic2-super-tabs'
@NgModule({
  declarations: [
    DashbordShoppingPage,
  ],
  imports: [ SuperTabsModule,
    IonicPageModule.forChild(DashbordShoppingPage),
  ],
})
export class DashbordShoppingPageModule {}
