import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { SuperTabs } from "../../component/scroll-tabs/src/components/super-tabs";
/**
 * Generated class for the DashbordShoppingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashbord-shopping',
  templateUrl: 'dashbord-shopping.html',
})
export class DashbordShoppingPage {

  @ViewChild(SuperTabs) superTabs: SuperTabs;
  page1 = HomePage;
  page2 = HomePage;
  page3 = HomePage;
  page4 = HomePage;
  page5 = HomePage;

  images = [

    { "imgs": "assets/imgs/select_country.PNG" },


  ]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashbordShoppingPage');
  }
  onTabSelect(ev: any) {
    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
  }
}
