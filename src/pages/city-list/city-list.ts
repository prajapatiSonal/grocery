import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DashbordShoppingPage } from '../dashbord-shopping/dashbord-shopping';
// import { DashbordShoppingPage } from '../dashbord-shopping/dashbord-shopping';

/**
 * Generated class for the CityListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-city-list',
  templateUrl: 'city-list.html',
})
export class CityListPage {
  city_list = [
    { "name": "Surat" },
    { "name": "Valsad" },
    { "name": "Vapi" },
    { "name": "Navsari" },
    { "name": "Baroda" },
    { "name": "Jamnagar" },
    { "name": "Rajkot" },
    { "name": "Wakaner" },
    { "name": "Morbi" }
  ]
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  active(index) {
    for (let i = 0; i < this.city_list.length; i++) {
      this.city_list[i]['selected'] = false;
    }
    this.city_list[index]['selected'] = true;
    console.log(this.city_list);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CityListPage');
  }
  DoneClick(){
    this.navCtrl.setRoot(DashbordShoppingPage)
  }

}
